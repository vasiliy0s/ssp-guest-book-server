'use strict';

/**
 * Basic app model 'Comment'.
 */

var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var mongoosePaginate = require('mongoose-paginate');
var validate = require('mongoose-validator');

// Fixes 'mongoose-paginate' exception which causes when it using Object.assign.
require('object-assign-shim');

var textValidator = [
  // min length.
  validate({
    validator: 'isLength',
    arguments: '2',
    message: 'Text should be equal or greater than {ARGS[0]}',
  }),
];

var authorNameValidator = [
  // min/max length.
  validate({
    validator: 'isLength',
    arguments: [2, 50],
    message: 'Author name should be between {ARGS[0]} and {ARGS[1]} characters',
  }),
  
  // only letters.
  validate({
    validator: 'isAlpha',
    message: 'Name should contain only english letters',
  }),
];

var schema = mongoose.Schema({
  likes     : { type: Number, default: 0, },
  text      : { type: String, required: true, validate: textValidator, },
  authorName: { type: String, required: true, validate: authorNameValidator, },
});

// Add 'createdAt' and 'updatedAt' fields.
schema.plugin(timestamps);

// Add (static) pagination methods.
schema.plugin(mongoosePaginate);

// 'Like' method implementation.
schema.methods.like = function (cb) {
  this.likes++;
  return this.save(cb);
};

// 'Dislike' method implementation.
schema.methods.dislike = function (cb) {
  this.likes--;
  return this.save(cb);
};

var Comment = mongoose.model('Comment', schema);

module.exports = Comment;
