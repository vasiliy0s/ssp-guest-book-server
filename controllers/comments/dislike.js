'use strict';

/**
 * This controller only decrement special value 'likes' in Comment model.
 */

var DislkeCommentController = function (req, res) {
  var Comment = res.app.models.comment;
  var params = req.params;
  
  if (!params.id) {
    return res.status(404).send('Comment not found');
  }
  
  Comment.findById(params.id)
    .exec(function (err, comment) {
      if (err) {
        if (err.name === 'CastError') {
          return res.status(404).send('Comment not found');
        }
        
        return res.status(500).json(err);
      }
      
      if (!comment) {
        return res.status(404).send('Comment not found');
      }
      
      // Decrement likes.
      comment.likes--;
      
      comment.save(function (err) {
        if (err) {
          return res.status(500).json(err);
        }
        
        res.status(202).send('Comment was disliked');
      });
    });
};

module.exports = DislkeCommentController;
