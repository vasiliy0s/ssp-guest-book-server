'use strict';

/**
 * This controller only increments special value 'likes' in Comment model.
 */

var LikeCommentController = function (req, res) {
  var Comment = res.app.models.comment;
  var params = req.params;
  
  if (!params.id) {
    return res.status(404).send('Comment not found');
  }
  
  Comment.findById(params.id)
    .exec(function (err, comment) {
      if (err) {
        if (err.name === 'CastError') {
          return res.status(404).send('Comment not found');
        }
        
        return res.status(500).json(err);
      }
      
      if (!comment) {
        return res.status(404).send('Comment not found');
      }
      
      // Increment likes.
      comment.likes++;
      
      comment.save(function (err) {
        if (err) {
          return res.status(500).json(err);
        }
        
        res.status(202).send('Comment was liked');
      });
    });
};

module.exports = LikeCommentController;
