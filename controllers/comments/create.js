'use strict';

var CreateCommentController = function (req, res) {
  var Comment = res.app.models.comment;
  
  // Fields whitelist.
  var body = req.body;
  var data = {
    likes     : body.likes,
    text      : body.text,
    authorName: body.authorName,
  };
  
  var comment = new Comment(data);
  
  comment.save(function (err) {
    if (err) {
      return res.status(500).json(err);
    }
    
    console.log('New comment succesfully saved:', comment);
    
    res.status(201).json(comment);
  });
};

module.exports = CreateCommentController;
