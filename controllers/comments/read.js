'use strict';

var ReadCommentController = function (req, res) {
  var Comment = res.app.models.comment;
  var params = req.params;
  
  if (!params.id) {
    return res.status(404).text('Cannot find Comment for read');
  }
  
  Comment.findById(params.id)
    .exec(function (err, comment) {
      if (err) {
        if (err.name === 'CastError') {
          return res.status(404).send('Comment not found');
        }
        
        return res.status(500).json(err);
      }
      
      if (!comment) {
        return res.status(404).send('Comment not found');
      }
      
      res.status(202).json(comment);
    });
};

module.exports = ReadCommentController;
