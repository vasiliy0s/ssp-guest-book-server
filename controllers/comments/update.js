'use strict';

var UpdateCommentController = function (req, res) {
  var Comment = res.app.models.comment;
  var params = req.params;
  
  if (!params.id) {
    return res.status(404).text('Cannot find Comment for read');
  }
  
  // Pass only acceptable fields.
  var body = req.body;
  var updates = {
    text      : body.text,
    authorName: body.authorName,
  };
  
  Comment.findById(params.id)
    .exec(function (err, comment) {
      if (err) {
        if (err.name === 'CastError') {
          return res.status(404).send('Comment not found');
        }
        
        return res.status(500).json(err);
      }
      
      if (!comment) {
        return res.status(404).send('Comment not found');
      }
      
      var changed = false;
      for (var pp in updates) {
        if (body.hasOwnProperty(pp)) {
          comment[pp] = updates[pp];
          changed = true;
        }
      }
      
      if (!changed) {
        return res.status(204).send('Nothing to change');
      }
      
      comment.save(function (err) {
        if (err) {
          return res.status(500).json(err);
        }
        
        console.log('Comment successfully changed:', comment);
        
        res.status(202).json(comment);
      });
    });
};

module.exports = UpdateCommentController;
