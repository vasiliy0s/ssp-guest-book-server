'use strict';

var requireDir = require('require-dir');
var cors = require('cors');
var router = require('express').Router();

var controllers = requireDir('./comments');

router
  // CORS headers presentation
  .use(cors())
  
  // GET /
  .get('/', controllers.index)
  
  // POST /
  .post('/', controllers.create)
  
  // GET /569a7e3629c7f04d6faeb8bf
  .get('/:id', controllers.read)
  
  // PATCH /569a7e3629c7f04d6faeb8bf/like
  .patch('/:id/like', controllers.like)
  
  // PATCH /569a7e3629c7f04d6faeb8bf/dislike
  .patch('/:id/dislike', controllers.dislike)
  
  // PATCH /569a7e3629c7f04d6faeb8bf
  .patch('/:id', controllers.update)
  
  // DELETE /569a7e3629c7f04d6faeb8bf
  .delete('/:id', controllers.delete);

module.exports = router;
